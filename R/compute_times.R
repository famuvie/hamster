#' Compute time in hours spent in tasks
#'
#' Modify a table of facts by converting start and end times to POSIXct times,
#' compute start date and total time in hours and in weeks.
#'
#' @param x data.frame-like table with facts
#'
#' @return A local tibble. Even if the source is within a database, we can't
#' compute times in the database in general.
#' @export
#' @import dplyr
compute_times <- function(x) {
  x %>%
    ## Needed to allow use of R-only functions like strptime and difftime
    collect() %>%
    mutate(
      ## Date-time class variables
      start_time = as.POSIXct(strptime(start_time, "%Y-%m-%d %H:%M:%S")),
      end_time = as.POSIXct(strptime(end_time, "%Y-%m-%d %H:%M:%S")),
      start_date = as.Date(start_time),
      time_hours = difftime(end_time, start_time, units = "hours"),
      ## Formule 1 : 35h 45m / semaine
      ## Formule 2 : 37h 30m / semaine
      time_wks = as.numeric(time_hours) / 37.5
    )
}
