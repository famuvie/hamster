
<!-- README.md is generated from README.Rmd. Please edit that file -->

# hamster

<!-- badges: start -->

<!-- badges: end -->

Query a [hamster](http://projecthamster.org/) database of time-tracking
data to perform searches, data-analysis or modifications.

## Installation

<!-- You can install the released version of hamster from [CRAN](https://CRAN.R-project.org) with: -->

<!-- ``` r -->

<!-- install.packages("hamster") -->

<!-- ``` -->

``` r
if (!require("remotes")) {
  install.packages("remotes")
}
remotes::install_gitlab("famuvie/hamster", host = "forgemia.inra.fr")
```

## Example

You need to use [hamster](http://projecthamster.org/) and locate the
database file, named `hamster.db`

``` r
library(hamster)
suppressPackageStartupMessages(
  library(dplyr)
)

## Establish a "connection" to your database
## By default, it looks for the database file at .local/share/hamster-applet
hdb <- hamster_connect()
```

The database has the following relational structure:

![](man/figures/data_model.png)

You can use functions from package [`DBI`](https://dbi.r-dbi.org/) to
query the database at low-level.

``` r
DBI::dbListTables(hdb)
#>  [1] "activities"          "categories"          "fact_index"         
#>  [4] "fact_index_content"  "fact_index_segdir"   "fact_index_segments"
#>  [7] "fact_tags"           "facts"               "sqlite_sequence"    
#> [10] "tags"                "version"
```

However, the purpose of the package `hamster` is to provide
domain-specific functions to facilitate the interaction with this
particular database.

``` r
activities(hdb)
#> # Source:   lazy query [?? x 3]
#> # Database: sqlite 3.30.1 [/home/facu/Work/logistica/hamster.db]
#>    activity_id activity             category_id
#>          <int> <chr>                      <int>
#>  1           8 forest-genetics                1
#>  2           9 clavelee                       1
#>  3          11 breedR                         1
#>  4          15 CAPICHE                        1
#>  5          16 setup_infra                    5
#>  6          17 ppr-senegal                    1
#>  7          18 newcastle-madagascar           1
#>  8          19 medical                        5
#>  9          20 ppp-oie                        1
#> 10          21 Culicoides-friction            1
#> # … with more rows

## Get all "facts" with a given tag
facts_rforum <- 
  fact_tags(hdb) %>% 
  filter(tag == "support") %>% 
  left_join(
    hamster_joint(hdb),
    by = c(fact_id = "id")
  )

## Which activities/categories are tagged support and how much time has been
## devoted to them.
facts_rforum %>%
  compute_times() %>%  # from this point on, the data are locally stored
  group_by(activity, category) %>%
  summarise(
    time_hours = sum(time_hours)
  )
#> `summarise()` regrouping output by 'activity' (override with `.groups` argument)
#> # A tibble: 13 x 3
#> # Groups:   activity [13]
#>    activity           category     time_hours
#>    <chr>              <chr>        <drtn>    
#>  1 AMercier           Supervision   2.0 hours
#>  2 asf_spread_belguim Projects      2.5 hours
#>  3 breedR             Projects     10.0 hours
#>  4 btb-southafrica    Projects      4.0 hours
#>  5 CAPICHE            Projects      8.0 hours
#>  6 Consulting-misc    Projects      8.0 hours
#>  7 dataverse-astre    Organisation  2.0 hours
#>  8 EuFMD              Projects      2.0 hours
#>  9 forum_R            Outreach     15.5 hours
#> 10 mapmcda            Projects      1.0 hours
#> 11 praps              Projects     19.0 hours
#> 12 SNintyonyo         Supervision   2.0 hours
#> 13 YBarry             Supervision   5.0 hours

## Change the activity for the given facts
## Looks like we need to use SQL syntax for this
## https://stackoverflow.com/questions/45149027/can-i-run-an-sql-update-statement-using-only-dplyr-syntax-in-r
# query_tbl(hdb, "facts", facts_rforum %>% pull(fact_id)) %>% 
#   mutate(
#     activity_id = activities(hdb) %>% 
#       filter(activity == "forum_R") %>% 
#       pull(activity_id)
#   )
```
